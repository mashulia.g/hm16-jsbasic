let firstNumber = +prompt('Enter FIRST number');
while(!firstNumber || firstNumber == "") {
    firstNumber = +prompt('Enter FIRST number AGAIN', '');
}
let secondNumber = +prompt('Enter SECOND number');

while(!secondNumber || secondNumber == "") {
    secondNumber = +prompt('Enter SECOND number AGAIN', '');
}
let number = +prompt('Enter number:');
while(!number || number == "") {
    number = +prompt('Enter number AGAIN', '');
}


function fibo (number, firstNumber,secondNumber) {
    if (number === 2){
        return secondNumber;
    } else if (number === 1){
        return firstNumber;
    } else {
        let f1 = firstNumber;
        let f2 = secondNumber;
        let f3;
        for (let i = 2; i < number; i++) {
            f3 = f1 + f2;
            f1 = f2;
            f2 = f3;
        }
        return f3;
    }
}
function fiboMinus (number, firstNumber,secondNumber) {

    let f1 = firstNumber, f2 = secondNumber, f3;
        for (let i = 2; i < number; i++) {
            f3 = f1 - f2;
            f1 = f2;
            f2 = f3;
        }
    return f3;
}

if (firstNumber > 0 && secondNumber > 0){
    alert(`Value of Fibonacci number is: ${fibo(number, firstNumber, secondNumber)}`);
} else {
    alert(`Value of Fibonacci number is: ${fiboMinus(number, firstNumber, secondNumber)}`);
}
